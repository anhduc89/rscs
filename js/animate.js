var scrolled = false;
$(document).ready(function () {
//    $('.menu-bar-li').hover(function () {
//        var y = $(this).attr('menu');
//        $('.dropdown-menu[menu = ' + y + ']').slideDown();
//        $('.dropdown-menu[menu != ' + y + ']').hide();
//    });
    $('.mobile-menu').click(function () {
        var y = $(this).attr('sub');

        $('.sub-menu[sub = ' + y + ']').slideToggle();
        $('.sub-menu[sub = ' + y + ']').toggleClass('active');
        $('.sub-menu[sub != ' + y + ']').removeClass('active');
        $('.sub-menu[sub != ' + y + ']').hide();
        if ($('.sub-menu[sub = ' + y + ']').hasClass('active') === true) {
            $('.border-mobile-menu').addClass('active');
            $('.border-mobile-menu').attr('sub', y);
            $('.mobile-menu[sub != ' + y + ']').css('opacity', '0.3');
            $('.mobile-menu[sub = ' + y + ']').css('opacity', '1');
            $('body').bind('touchmove', function (e) {
                e.preventDefault();
            });
        } else {
            $('.mobile-menu').css('opacity', '1');
            $('.border-mobile-menu').removeClass('active');
            $('.border-mobile-menu').removeAttr('sub');
            $('body').unbind('touchmove');
        }
    });
    $(document).on('click', '.border-mobile-menu', function () {
        if ($(this).hasClass('active') === true) {
            $('.border-mobile-menu').removeClass('active');
            $('.sub-menu').slideUp();
            $('.sub-menu').removeClass('active');
            $('.mobile-menu').css('opacity', '1');
        }

    });
    $('.content-slider-warning').click(function(){
        var x = $(this).attr('data');
        $('.content-slider-warning[data = ' + x + ']').addClass('active');
        $('.content-slider-warning[data != ' + x + ']').removeClass('active');
        $('.img-full-warning[data = ' + x + ']').show();
        $('.img-full-warning[data != ' + x + ']').hide();
    });
//    $(document).mouseup(function (e){
//        var container = $('.menu-bar-li');
//        if (!container.is(e.target)
//                && container.has(e.target).length === 0)
//        {
//            container.hide();
//        }
//    });
});

