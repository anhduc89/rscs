<?php
    class Home_m extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function getProductById($id_product)
        {
            $this->db->select("*");
            $this->db->from("product");
            $this->db->where("id_product", $id_product);
            $query = $this->db->get();
            return $query->row_array();
        }

    }
    
?>