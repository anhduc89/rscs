<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_m');
	}
	public function index()
	{
		/*
			dữ liệu lấy bằng js bên v_home
		*/
		$this->load->view('v_head');
		$this->load->view('v_home');
		$this->load->view('v_footer');
	}
	// lấy tất cả các sản phẩm 
	public function all_available_products()
	{
		$this->load->view('v_head_menu');
		$this->load->view('v_all_products');
		$this->load->view('v_footer');
	}
	// lấy chi tiết sản phẩm 
	public function detailProduct() 
	{
		$data['id_product'] = $this->uri->segment(2);
		$this->load->view('v_head_menu');
		$this->load->view('v_detail_product',$data);
		$this->load->view('v_footer');
	}

	// lấy camera 
	public function camera()
	{
		$this->load->view('v_head_menu');
		$this->load->view('v_camera');
		$this->load->view('v_footer');
	}

	// xem camera 
	public function watchCamera()
	{
		$data['id_camera']  = $this->uri->segment(2);
		$this->load->view('v_head_menu');
		$this->load->view('v_watch', $data);
		$this->load->view('v_footer');
	}

	// danh sách cửa hàng 
	public function shop()
	{
		$this->load->view('v_head_menu');
		$this->load->view('v_shop');
		$this->load->view('v_footer');
	}
	// giới thiệu
	public function introduce()
	{
		$this->load->view('v_head_menu');
		$this->load->view('v_introduce');
		$this->load->view('v_footer');
	}
	// liên hệ 
	public function contact()
	{
		$this->load->view('v_head_menu');
		$this->load->view('v_contact');
		$this->load->view('v_footer');
	}
	// tuyển dụng 
	public function recruitment()
	{
		$this->load->view('v_head_menu');
		$this->load->view('v_recruitment');
		$this->load->view('v_footer');
	}
	// tìm kiếm 
	
}