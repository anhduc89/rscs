<div class="gioi-thieu  body-container">
    <div class="img-introduce"> 
            <div class="container-fluid">
                <div class="row">
                        <div class="col-md-12">
                            <div class="img-contact-header">
                                <h1>Về chúng tôi</h1>
                            </div>
                        </div>
                </div>
            </div>
    </div>

    <div class="aboutme">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="text-aboutme">
                        <h3>Rau sạch Chúc Sơn</h3>
                        <p>Phát triển mô hình trồng rau an toàn, cung cấp những sản phẩm bảo đảm chất lượng đến tay người tiêu dùng là những gì 
                            HTX Rau sạch Chúc Sơn (Chương Mỹ - Hà Nội) đang làm, khi tận dụng được lợi thế về diện tích, khí hậu, nguồn nước tại địa phương.</p>
                        <p>Rau của HTX phát triển và bảo đảm chất lượng, đáp ứng nhu cầu người tiêu dùng. Rau được ký hợp đồng và bán sau khi đã đóng gói.</p>
                        <p>Chúng tôi áp dụng công nghệ truy xuất và minh bạch nông sản <a href="http://evietgap.com">e-VIETGAP</a> để chứng minh với người tiêu dùng về sự minh bạch, uy tín của chúng tôi.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sumenh">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-sumenh">
                        <div class="col-md-8 col-md-offset-2">
                            <h1> Mong muốn của chúng tôi </h1>
                            <h3> Mỗi  bữa ăn của từng gia đình đều là bữa ăn ngon và sạch </h3>
                            <p class="hidden-sm hidden-xs "> Và chúng tôi đang cố gắng nỗ lực từng ngày để thực hiện được điều đó. Mỗi sản phẩm của chúng tôi là một sự tâm huyết, trách nhiệm mong muốn góp phần làm cho xã hội tốt đẹp hơn.</p> 
                            <p class="hidden-sm hidden-xs "> Vấn đề  vệ sinh an toàn thực phẩm là vấn đề nhức nhối của xã hội hiện nay, chúng tôi đã, đang và sẽ góp phần công sức nhỏ bé của mình để cùng xã hội giải quyết vấn đề đó.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <div class="nhanvien">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12">
                    <div class="text-sumenh">
                        <h1>Đội Ngũ Nhân Viên</h1>
                        <p>
                            Chúng tôi là tập hợp của các kỹ sư nông nghiệp sáng tạo và những người lãnh đạo lâu năm có kinh nghiệm trong lĩnh vực nông nghiệp.
                            Chúng tôi đem những ứng dụng khoa học kỹ thuật mới nhất áp dụng vào sản xuất để đảm bảo được sản phẩm đưa ra thị trường là sản phẩm rau an toàn.      
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="team">
                            <div class="img-team">
                                <img src="<?php echo base_url();?>img/phu.jpg" alt="">
                            </div>
                            <div class="name-team">
                                    <p class="tbold">Bùi Minh Phú</p>
                                    <p>Kỹ sư nông nghiệp    </p>                                  
                            </div>
                    </div>

                    <div class="info-team">
                        <span>
                            Mình được tham gia vào một sứ mệnh làm ra sản phẩm an toàn, thân thiện môi trường và hợp tác cùng bà con nông dân để dần dần thay đổi ý thức của họ. 
                            Có rất nhiều khó khăn nhưng mình tin tưởng rằng bằng việc hợp tác,dần dần việc sản xuất an toàn sẽ trở nên dễ dàng và mở rộng hơn 
                        </span>
                    </div>
                    
                </div>

                <div class="col-md-6 col-sm-6 ">
                    <div class="team">
                            <div class="img-team">
                                <img src="<?php echo base_url();?>img/phu.jpg" alt="">
                            </div>
                            <div class="name-team">
                                    <p class="tbold">Bùi Minh Phú</p>
                                    <p>Kỹ sư nông nghiệp    </p>                                  
                            </div>
                    </div>

                    <div class="info-team">
                        <span>
                            Mình được tham gia vào một sứ mệnh làm ra sản phẩm an toàn, thân thiện môi trường và hợp tác cùng bà con nông dân để dần dần thay đổi ý thức của họ. 
                            Có rất nhiều khó khăn nhưng mình tin tưởng rằng bằng việc hợp tác,dần dần việc sản xuất an toàn sẽ trở nên dễ dàng và mở rộng hơn 
                        </span>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    

</div>