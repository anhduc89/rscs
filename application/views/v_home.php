
    <div class="container">
        <div class="products-sale">
            <div class="row" id="title-products"> 
                <h2>Các sản phẩm đang bán</h2>
            </div>

            <div class="row" id="product_container">


            </div>
            <div class="row" id="title-products"> 
                <span><a href="<?php echo base_url();?>danh-sach-san-pham"> &raquo Xem tất cả sản phẩm  </a></span>
            </div>
            

        </div>
    </div>
    <div class="introduce">
                <div class="container">
                    <div class="row">
                        <div class="media">
                            <h2>Truyền thông nói về chúng tôi</h2>
                            <div class="col-md-6">
                                <iframe width='100%' height='315' src="https://www.youtube.com/embed/25QViQg3Cyc" frameborder="0" allowfullscreen></iframe>
                                <p>VTV3 Vì một tương lai xanh : Áp dụng Quy trình truy xuất, minh bạch e-VIETGAP tại cơ sở HTX CHÚC SƠN</p>
                            </div>

                            <div class="col-md-6">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/FeeioUSjLtk" frameborder="0" allowfullscreen></iframe>
                                <p> ANTV Kinh tế tiêu dùng : Quy trình truy xuất, minh bạch e-VIETGAP</p>
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="row">                             
                                    <h2>Tại sao nên dùng rau sạch của HTX Chúc Sơn</h2>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="icon-introduce">
                                                <!--<img src="<?php echo base_url()?>img/lg.png" alt="">-->
                                            </div>
                                            <div class="text-introduce">
                                                <span>
                                                    Cơ sở sản xuất rau Chúc Sơn đã được chứng nhận tiêu chuẩn 
                                                    <a href="http://www.vietgap.com/enterprise/1015_7298/hop-tac-xa-rau-qua-sach-chuc-son.html" target="_blank"> VietGAP-TT-13-03-01-0065</a> và tham gia quy trình eVietGAP giúp quản lý và truy xuất 
                                                    nguồn gốc rau an toàn.
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <div class="icon-introduce">
                                                <!--<img src="<?php echo base_url()?>img/lg.png" alt="">-->
                                            </div>
                                            <div class="text-introduce">
                                                <span>Mọi quá trình sản xuất canh tác của HTX rau sạch Chúc Sơn đều được theo dõi bằng camera có độ phân giải cao, lưu trữ 30 ngày gần nhất giúp truy xuất một cách nhanh chóng hiệu quả.</span>
                                            </div>
                                        </div>

                                        <div class="col-md-4  col-sm-4">
                                            <div class="icon-introduce">
                                                <!--<img src="<?php echo base_url()?>img/lg.png" alt="">-->
                                            </div>
                                            <div class="text-introduce">
                                                <span>HTX rau sạch Chúc Sơn có đại lý phân phối rau sạch, giúp người tiêu dùng dễ dàng tiếp cận với sản phẩm. 
                                                    Chúng tôi hiện đang cung cấp cho các trường học, bệnh viện và các công ty trên địa bàn Chúc Sơn - Chương Mỹ - Hà Nội</span>
                                            </div>
                                        </div>
                            
                        </div>
                    </div>
                </div>

                            <!-- <div class="row">                             
                                            <h2>Tại sao nên dùng rau sạch của HTX Chúc Sơn</h2>
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="icon-introduce">
                                                       
                                                    </div>
                                                    <div class="text-introduce">
                                                        <span>
                                                            Cơ sở sản xuất rau Chúc Sơn đã được chứng nhận tiêu chuẩn 
                                                            <a href="http://www.vietgap.com/enterprise/1015_7298/hop-tac-xa-rau-qua-sach-chuc-son.html" target="_blank"> VietGAP-TT-13-03-01-0065</a> và tham gia quy trình eVietGAP giúp quản lý và truy xuất 
                                                            nguồn gốc rau an toàn.
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <div class="icon-introduce">
                                                        
                                                    </div>
                                                    <div class="text-introduce">
                                                        <span>Mọi quá trình sản xuất canh tác của HTX rau sạch Chúc Sơn đều được theo dõi bằng camera có độ phân giải cao, lưu trữ 30 ngày gần nhất giúp truy xuất một cách nhanh chóng hiệu quả.</span>
                                                    </div>
                                                </div>

                                                <div class="col-md-4  col-sm-4">
                                                    <div class="icon-introduce">
                                                       
                                                    </div>
                                                    <div class="text-introduce">
                                                        <span>HTX rau sạch Chúc Sơn có đại lý phân phối rau sạch, giúp người tiêu dùng dễ dàng tiếp cận với sản phẩm. 
                                                            Chúng tôi hiện đang cung cấp cho các trường học, bệnh viện và các công ty trên địa bàn Chúc Sơn - Chương Mỹ - Hà Nội</span>
                                                    </div>
                                                </div>
                                    
                                </div>
                            </div>
            </div> -->

        </div>
    </div>







<script>    
            var base_url = "http://egap.vn/";
            var base_local = '<?php echo base_url();?>';

            $( document ).ready(function() {
                    $.ajax({
                        type: "GET",
                        url: "http://egap.vn/product/apiLimitProduct/2",
                        crossDomain: true,
                        contentType: "application/x-www-form-urlencoded",
                        async: false,
                        dataType: 'json',
                        processData: false,
                        cache: false,
                        success: function (data) {                            
                            var html = '';
                            for(var i = 0; i < data.length; i++)
                            {
                            
                                html += '<div class="col-md-3 col-xs-12 col-sm-6 ">';
                                    html += '<div class="product">';
                                        html +='<a href="   ' + base_local +'san-pham/'+data[i].id_product +'   "><img style="height:200px;width:auto; max-width:200px;" src="'+ base_url+  data[i].oimage + '" alt=""  class="img_product"></a>';
                                        html += '<p class="name-product"><a href=" ' + base_local +'san-pham/'+data[i].id_product +' ">' + data[i].pkname + '</a></p>';
                                        
                                        html += '<p class="date-product"> Ngày thu hoạch '+ data[i].last_date +' </p>';
                                       
                                        
                                        html += '<button><a href="   ' + base_local +'san-pham/'+data[i].id_product +'   ">&raquo Xem chi tiết</a></button>';
                                    html += "</div>";

                                html += "</div>";
                            }
                            $('#product_container').html(html);
                        },
                        error: function (ErrorResponse) {
                            //alert("error");
                        }
                    });
            });
            
</script>