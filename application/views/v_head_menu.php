<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Rau Sạch Chúc Sơn</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="<?php echo base_url(); ?>js/jquery-3.1.1.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/animate.js" type="text/javascript"></script>
	<!--<script type="text/javascript" src="<?php echo base_url(); ?>css/slick/slick.min.js"></script>-->
	<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css"/>     

</head>
<body>

<div class="wrapper">
<div class="container-fluid">
		
		<div class="menu-head menu-green">
			<div class="row  hidden-xs hidden-sm">
					<div class="col-md-3 col-sm-3">
						<div class="img-logo">
							<a href="<?php echo base_url();?>home"><img src="<?php echo base_url()?>/img/logo.png" alt=""></a>
						</div>
					</div>

					<div class="col-md-9 col-sm-9">
						<div class="list-menu">
							<ul>
								<li><a href="<?php echo base_url();?>home">Trang Chủ</a></li>
								<li><a href="<?php echo base_url();?>gioi-thieu">Giới Thiệu</a></li>
								<li><a href="<?php echo base_url();?>danh-sach-san-pham">Sản Phẩm</a></li>
								<li><a href="<?php echo base_url();?>danh-sach-camera">Camera</a></li>
								<li><a href="<?php echo base_url();?>danh-sach-cua-hang">Cửa Hàng</a></li>
								<li><a href="<?php echo base_url();?>lien-he">Liên Hệ</a></li>
								<li><a href="<?php echo base_url();?>tuyen-dung">Tuyển Dụng</a></li>
							</ul>
						</div>
					</div>
			</div>
		</div>

		<div class="menu-head menu-green">
			<div class="row hidden-md hidden-lg">
					<div class="col-md-3 col-sm-2 col-xs-3">
						<div class="img-logo">
							<a href="<?php echo base_url();?>home"><img src="<?php echo base_url()?>/img/logo.png" alt=""></a>
						</div>
					</div>

					<div class="col-md-9 col-sm-5 col-xs-9">
						<div class="list-menu">
							
							<i class="fa fa-bars fa-2x" aria-hidden="true" data-toggle="collapse" data-target="#demo" style="color:white"></i>

								<div id="demo" class="collapse">
										<ul>
											<li><a href="<?php echo base_url();?>home">Trang Chủ</a></li>
											<li><a href="<?php echo base_url();?>gioi-thieu">Giới Thiệu</a></li>
											<li><a href="<?php echo base_url();?>danh-sach-san-pham">Sản Phẩm</a></li>
											<li><a href="<?php echo base_url();?>danh-sach-camera">Camera</a></li>
											<li><a href="<?php echo base_url();?>danh-sach-cua-hang">Cửa Hàng</a></li>
											<li><a href="<?php echo base_url();?>lien-he">Liên Hệ</a></li>
											<li><a href="<?php echo base_url();?>tuyen-dung">Tuyển Dụng</a></li>
										</ul>
								</div>
						</div>
					</div>

			</div>
		</div>



</div>
</div>