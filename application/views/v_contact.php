<div class="wrapper">
    <div class="img-contact">
        <div class="container body-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="img-contact-header">
                        <h1>Liên Hệ</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="map-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-contact">
                            <img src="<?php echo base_url()?>img/lienhe.png" alt="">
                            <span>Hợp tác xã rau sạch Chúc Sơn</span>

                            <div class="text-content-map">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="<?php echo base_url();?>img/checkin.png" alt="">
                                        <span>Giáp Ngọ - Chúc Sơn - Huyện Chương Mỹ - Hà Nội</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="<?php echo base_url();?>img/phone.png" alt="">
                                        <span>02485887868</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="<?php echo base_url();?>img/website.png" alt="">
                                        <span>rausachchucson.com</span>
                                    </div>
                                </div>

                            </div>
                    </div>

                    <div class="text-contact">
                            <img src="<?php echo base_url()?>img/lienhe.png" alt="">
                            <span>Chăm sóc khách hàng </span>

                            <div class="text-content-map">
                                <div class="row">
                                    <div class="col-md-12">
                                        <i class="fa fa-envelope-o" aria-hidden="true" style="float: left; padding: 0px 5px; font-size: 25px;"> </i>
                                        <span>rausachchucson@gmail.com</span>
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                </div>

                <div class="col-md-7">
                    <div id="map" style="position: relative; overflow: hidden;height: 400px;">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script async  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBt8hhTg9BGaO_Ndllrffcgr4f2Ne6oZgw&callback=initMap" type="text/javascript"></script>
<script>

    function initMap(){
        // changePosition(17.10,106.7,6);

            changePosition(20.923754, 105.723378, 13);
    }
    
    function changePosition(m_lat,m_lng,m_zoom) { 
        var chucson = {lat: m_lat, lng: m_lng};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: m_zoom ,
            center: chucson
        });
        var marker = new google.maps.Marker({
            position: {
                lat: m_lat,
                lng: m_lng
            },
            map: map,
            title: "RAU SACH CHUC SON" 
        });
    }
</script>