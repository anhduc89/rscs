<div class="wrapper">
    <div class="img-contact">
        <div class="container body-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="img-contact-header">
                        <h1>Tuyển Dụng</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="map-contact tuyen-dung" >
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="text-contact">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>HTX rau quả sạch Chúc Sơn tuyển nhân viên kỹ thuật.</h4>
                                <p>
                                    HTX rau quả sạch Chúc Sơn tiên phong sử dụng tem truy xuất e-VIETGAP. 
                                    Cơ sở đang cung cấp rau sạch cho các bệnh viện lớn như Bạch Mai - Hà Nội và các trường học trên địa bàn huyện Chương Mỹ.
                                    Cơ sở sản xuất đạt tiêu chuẩn VIETGAP số <a href="http://www.vietgap.com/enterprise/1015_7298/hop-tac-xa-rau-qua-sach-chuc-son.html">VietGAP-TT-13-03-01-0065</a> .<br/>
                                    Chúng tôi cần tuyển dụng vị trí sau:
                                </p>
                                <h1><b> 02 Nhân Viên Kỹ Thuật </b></h1>
                                <p><b >  * Yêu cầu trình độ chuyên môn </b>: Kỹ sư chuyên ngành trồng trọt, bảo vệ thực vật, sẽ được đào tạo thêm.</p>
                                <p><b >* Mô tả công việc: </b>
                                    Giám sát các hộ sản xuất, lên kế hoạch gieo trồng và thu mua, kiểm tra và chịu trách nhiệm chất lượng sản phẩm cũng như truy xuất ngồn gốc rõ ràng...
                                </p>
                                <p><b> * Lương </b>: theo thỏa thuận khi phỏng vấn </p>
                                <p><b> * Chế độ </b>: được hưởng đầy đủ chế độ BHXH, BHYT, BHTN.... theo luật lao động </p>
                                <p><b> * Địa chỉ làm việc </b> :  Nhà sơ chế RAT - Thôn Giáp Ngọ - TT Chúc Sơn - Chương Mỹ - Hà Nội</p>
                                <p><b> * CV gửi về email :  </b>  rausachchucson@gmail.com  hoặc Nhà sơ chế RAT - Thôn Giáp Ngọ - TT Chúc Sơn - Chương Mỹ - Hà Nội </p>
                                <p><b> * Liên hệ :  </b>  Ms Hiền: 0975.655.507 </p>
                            </div>
                        </div>

                    </div>                    
                </div>
                
            </div>
        </div>
    </div>
</div>