<div class="container body-container">
    <div class="products-sale">
        <div class="row" id="title-shops"> 
            <h2>Danh sách các cửa hàng</h2>
        </div>
        
        <div class="row" id="shops"> </div>
    </div>

</div>

<script>    
            var base_url = "http://evietgap.com/";
            var base_local = '<?php echo base_url();?>';
            $.ajax({
                type: "GET",
                url: "http://evietgap.com/product/apiShop/2",
                crossDomain: true,
                contentType: "application/x-www-form-urlencoded",
                async: false,
                dataType: 'json',
                processData: false,
                cache: false,
                success: function (data) {
                    var html = '';
                    
                    html += '<table class="table table-bordered">';
                        html += ' <thead>';
                            html += ' <tr>';
                                html += ' <th   style="text-align:center;">Tên cửa hàng</th>';
                                html += ' <th   style="text-align:center;">Địa chỉ</th>';
                                html += ' <th   style="text-align:center;">Số điện thoại</th>';
                            html += ' </tr>';
                    html += ' </thead>';
                    for(var i = 0; i < data.length; i++)
                    {
                            html += ' <tbody>';
                                    html += ' <tr>';
                                        html += '<td>'+data[i].shop_name+'</td>';
                                        html += '<td>'+data[i].shop_address+'</td>';
                                        html += '<td>'+data[i].shop_phone+'</td>';
                                    html += '</tr>';
                            html += ' </tbody>';
                    }
                    html += ' </table>';
                    $('#shops').html(html);
                },
                error: function (ErrorResponse) {
                    alert("error");
                }
            });
</script>