<div class="wrapper">
<div class="container-fluid">
    <div class="introduce">
        <div class="contact">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 ">
                    <div class="detail-contact">
                        <div class="col-md-4  col-sm-6 col-xs-12">
                            <h3>Hợp tác xã rau sạch Chúc Sơn</h3>
                            <p> <i class="fa fa-map-marker" aria-hidden="true"></i>Giáp Ngọ - Chúc Sơn - Huyện Chương Mỹ - Hà Nội</p>
                            <p> <i class="fa fa-phone" aria-hidden="true"></i>02485887868 <br> 
                                   <i class="fa fa-envelope-o" aria-hidden="true"></i>rausachchucson@gmail.com <br> 
                                <i class="fa fa-globe" aria-hidden="true"></i>rausachchucson.com</p>
                        </div>
                        <div class="col-md-2  col-sm-2 hidden-xs hidden-sm">
                            <h3>Giới thiệu</h3>
                            <a href="<?php echo base_url()?>home/introduce"> <p><i class="fa fa-user" aria-hidden="true"></i> Về chúng tôi</p></a>
                        </div>
                         <div class="col-md-2  col-sm-2 hidden-xs hidden-sm">
                            <h3>Hỗ trợ</h3>
                            <p><i class="fa fa-credit-card" aria-hidden="true"></i>Mua hàng</p>
                            
                        </div>
                        <div class="col-md-4  col-sm-6 col-xs-12 fb-social">
                            <div id="fb-root"></div>
                            <div class="fb-page" data-href="https://www.facebook.com/HTX-Rau-Qu%E1%BA%A3-S%E1%BA%A1ch-Ch%C3%BAc-S%C6%A1n-678595265655444//" data-tabs="timeline" data-height="200px" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/rausachchucson/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/rausachchucson/">Rau Sạch Chúc Sơn</a></blockquote></div>
                             <div class="social">
                                    <a href="https://www.facebook.com/HTX-Rau-Qu%E1%BA%A3-S%E1%BA%A1ch-Ch%C3%BAc-S%C6%A1n-678595265655444/" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a><i class="fa fa-twitter"></i></a>
                                    <a href="https://gmail" target="_blank"><i class="fa fa-google"></i></a>
                                    <a  href="https://www.youtube.com/watch?v=2V8rYgRAbEo"  target="_blank"><i class="fa fa-youtube"></i></a>
                                    <a><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId=1861920557415111";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>