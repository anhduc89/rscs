<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Rau Sạch Chúc Sơn</title>

	<script src="<?php echo base_url(); ?>js/jquery-3.1.1.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/animate.js" type="text/javascript"></script>

	<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css"/>     

</head>
<body>
	<div class="wrapper">
	<div class="container-fluid">
		<div class="menu-head">
			<div class="row hidden-xs hidden-sm">
					<div class="col-md-3 col-sm-3">
						<div class="img-logo">
							<a href="<?php echo base_url();?>home"><img src="<?php echo base_url()?>/img/logo.png" alt=""></a>
						</div>
					</div>

					<div class="col-md-9 col-sm-9">
						<div class="list-menu">
							<ul>
								<li><a href="<?php echo base_url();?>home">Trang Chủ</a></li>
								<li><a href="<?php echo base_url();?>gioi-thieu">Giới Thiệu</a></li>
								<li><a href="<?php echo base_url();?>danh-sach-san-pham">Sản Phẩm</a></li>
								<li><a href="<?php echo base_url();?>danh-sach-camera">Camera </a></li>
								<li><a href="<?php echo base_url();?>danh-sach-cua-hang">Cửa Hàng</a></li>
								<li><a href="<?php echo base_url();?>lien-he">Liên Hệ</a></li>
								<li><a href="<?php echo base_url();?>tuyen-dung">Tuyển Dụng</a></li>
							</ul>
						</div>
					</div>
			</div>
		</div>

		<div class="menu-head">
			<div class="row hidden-md hidden-lg">
					<div class="col-md-3 col-sm-2 col-xs-3">
						<div class="img-logo">
							<a href="<?php echo base_url();?>home"><img src="<?php echo base_url()?>/img/logo.png" alt=""></a>
						</div>
					</div>

					<div class="col-md-9 col-sm-5 col-xs-9">
						<div class="list-menu">
							
								<i class="fa fa-bars fa-2x" aria-hidden="true" data-toggle="collapse" data-target="#demo" style="color:white"></i>
								<div id="demo" class="collapse">
										<ul>
											<li><a href="<?php echo base_url();?>home">Trang Chủ</a></li>
											<li><a href="<?php echo base_url();?>gioi-thieu">Giới Thiệu</a></li>
											<li><a href="<?php echo base_url();?>danh-sach-san-pham">Sản Phẩm</a></li>
											<li><a href="<?php echo base_url();?>danh-sach-camera">Camera</a></li>
											<li><a href="<?php echo base_url();?>danh-sach-cua-hang">Cửa Hàng</a></li>
											<li><a href="<?php echo base_url();?>lien-he">Liên Hệ</a></li>
											<li><a href="<?php echo base_url();?>tuyen-dung">Tuyển Dụng</a></li>
										</ul>
								</div>
						</div>
					</div>
			</div>
		</div>

		<div class="banner">
			<div class="row">
			
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
							<ol class="carousel-indicators">
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								<li data-target="#myCarousel" data-slide-to="1"></li>
								<li data-target="#myCarousel" data-slide-to="2"></li>
								<li data-target="#myCarousel" data-slide-to="3"></li>
							</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div class="item active">
								<img src="<?php echo base_url()?>/img/banner1.jpg" alt="Los Angeles" style="width:100%;">
								<a class=" hidden-xs hidden-sm" href="<?php echo base_url();?>danh-sach-camera"><button type="button" class="btn btn-success">Camera Giám Sát</button></a>
							</div>

							<div class="item">
								<img src="<?php echo base_url()?>/img/banner.jpg" alt="Camera" style="width:100%;">
								<a class=" hidden-xs hidden-sm" href="<?php echo base_url();?>danh-sach-san-pham"><button type="button" class="btn btn-primary">Sản Phẩm Đang Bán</button></a>
							</div>
						
							<div class="item">
								<img src="<?php echo base_url()?>/img/banner2.jpg" alt="Shop" style="width:100%;">
								<a class=" hidden-xs hidden-sm" href="<?php echo base_url();?>danh-sach-cua-hang"><button type="button" class="btn btn-danger">Danh Sách Cửa Hàng</button></a>
							</div>
							<div class="item">
								<img src="<?php echo base_url()?>/img/banner3.jpg" alt="Scan E-VIETGAP" style="width:100%;">
								
							</div>
						</div>

						<!-- Left and right controls -->
				        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				            <span class="sr-only">Previous</span>
				        </a>
				        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				            <span class="sr-only">Next</span>
				        </a>

						
					</div>
				
			</div>
		</div>

		<div class="search_product">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="text_search">
						<h2>Kiểm tra nguồn gốc sản phẩm</h2>
						<div class="input-group input-group-lg info_search">
							<input type="text" class="form-control text_code" placeholder="Nhập 16 kí tự trên tem truy xuất"  id="search-text">
							<button class="btn btn-success subimit_code" id="search-product">Truy xuất</button>
						</div>
						<span>Xem thêm hướng dẫn <a href="" data-toggle="modal" data-target="#myModal">tại đây</a></span>
					</div>
				</div>
			</div>
		</div>

		<div class="products-sale"></div>
	</div>

	<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hướng dẫn </h4>
      </div>
      <div class="modal-body">
        <p>Bạn hãy nhập 16 số kí tự trên tem truy xuất.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
      </div>
    </div>

  </div>
</div>
</div>
<script>

$( document ).ready(function() {

    $('#search-product').on('click', function () {
        var search_text = $('#search-text').val();
		var base_url = '<?php echo base_url();?>';
             $.ajax({
					url: 'http://evietgap.com/searchCode',
					type: "post",
					//dataType: "json",
					data: {
						qr_id: search_text
					},
					success: function(result) {
						console.log(result);
						if(result.success == 0)
						{
							$('.note').html(result.error);
						}
						else
						{
							window.location = result.link;
						}
					}
            });
     })
});

</script>